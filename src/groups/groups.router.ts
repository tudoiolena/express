import { Router } from 'express';
import * as groupsController from './groups.controller';
import controllerWrapper from '../application/unilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { groupCreateSchema, groupUpdateSchema } from './group.schema';
import { idParamSchema } from '../application/schemas/id-param-schema';

const router = Router();

router.get('/', controllerWrapper(groupsController.getAllGroupsWithStudents));

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupsController.getGroupById),
);

router.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup),
);

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(groupsController.updateGroupById),
);

router.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupsController.deleteGroupById),
);

export default router;

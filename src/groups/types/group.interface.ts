import { IStudent } from '../../students/types/student.interface';

export interface IGroup {
  id: string;
  name: string;
}

export interface IGroupWithStudents extends IGroup {
  students?: IStudent[];
}

import { Request, Response } from 'express';
import * as groupsService from './groups.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupUpdateRequest } from './types/group-update-request.interface';
import { IGroupCreateRequest } from './types/group-create-request.interface';

export const getAllGroupsWithStudents = (
  request: Request,
  response: Response,
) => {
  const groups = groupsService.getAllGroupsWithStudents();
  response.status(201).json(groups);
};

export const getGroupById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.getGroupById(id);
  response.status(201).json(group);
};

export const createGroup = (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = groupsService.createGroup(request.body);
  response.status(201).json(group);
};

export const updateGroupById = (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.updateGroupById(id, request.body);
  response.status(201).json(group);
};

export const deleteGroupById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = groupsService.deleteGroupById(id);
  response.status(201).json(group);
};

import HttpStatuses from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import * as groupsModel from './groups.model';
import { IGroup, IGroupWithStudents } from './types/group.interface';
import { getAllStudents } from '../students/students.model';

export const getAllGroupsWithStudents = (): IGroupWithStudents[] => {
  try {
    const groupsWithStudents: IGroupWithStudents[] = groupsModel
      .getAllGroups()
      .map((group) => {
        const students = getAllStudents().filter(
          (student) => student.groupId === group.id,
        );
        return { ...group, students };
      });
    return groupsWithStudents;
  } catch (error) {
    throw new HttpException(
      HttpStatuses.INTERNAL_SERVER_ERROR,
      'Something went wrong',
    );
  }
};

export const getGroupById = (id: string) => {
  const group = groupsModel.getGroupById(id);

  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  const students = getAllStudents().filter((student) => student.groupId === id);

  return { ...group, students };
};

export const createGroup = (groupCreateSchema: Omit<IGroup, 'id'>) => {
  return groupsModel.createGroup(groupCreateSchema);
};

export const updateGroupById = (
  id: string,
  groupUpdateSchema: Partial<IGroup>,
) => {
  return groupsModel.updateGroupById(id, groupUpdateSchema);
};

export const deleteGroupById = (id: string) => {
  return groupsModel.deleteGroupById(id);
};

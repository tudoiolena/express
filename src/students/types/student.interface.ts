export interface IStudent {
  id: string;
  name: string;
  surname: string;
  age: number;
  email: string;
  imagePath: string | null;
  groupId: string | null;
}

export interface IStudentWithGroupName extends IStudent {
  groupName?: string | null;
}

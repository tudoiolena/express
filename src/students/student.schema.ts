import Joi from 'joi';
import { IStudentWithGroupName } from './types/student.interface';

export const studentCreateSchema = Joi.object<
  Omit<IStudentWithGroupName, 'id'>
>({
  name: Joi.string().required(),
  surname: Joi.string().required(),
  age: Joi.number().required(),
  email: Joi.string().required(),
  imagePath: Joi.string().allow(null).required(),
  groupId: Joi.string().allow(null).required(),
});

export const studentUpdateSchema = Joi.object<Partial<IStudentWithGroupName>>({
  name: Joi.string().optional(),
  surname: Joi.string().optional(),
  age: Joi.number().optional(),
  email: Joi.string().optional(),
  imagePath: Joi.string().allow(null).optional(),
  groupId: Joi.string().allow(null).optional(),
});

export const addStudentToGroupSchema = Joi.object({
  studentId: Joi.string().required(),
  groupId: Joi.string().required(),
});

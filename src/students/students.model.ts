import { IStudent, IStudentWithGroupName } from './types/student.interface';
import ObjectID from 'bson-objectid';

const students: IStudent[] = [
  {
    id: ObjectID().toHexString(),
    name: 'John',
    surname: 'Depp',
    age: 22,
    email: 'johndepp@gmail.com',
    imagePath: null,
    groupId: null,
  },
  {
    id: ObjectID().toHexString(),
    name: 'Lisa',
    surname: 'Cudrow',
    age: 18,
    email: 'lisacudrow@gmail.com',
    imagePath: null,
    groupId: null,
  },
];

export const addInitialGroupName = (
  student: IStudentWithGroupName,
): IStudentWithGroupName => {
  return { ...student, groupName: null };
};

export const getAllStudents = (): IStudentWithGroupName[] => {
  return students;
};

export const getStudentById = (
  studentId: string,
): IStudentWithGroupName | undefined => {
  return students.find(({ id }) => id === studentId);
};

export const getStudentsByEmail = (
  studentEmail: string,
): IStudentWithGroupName | undefined => {
  return students.find(({ email }) => email === studentEmail);
};

export const createStudent = (
  createStudentSchema: Omit<IStudent, 'id'>,
): IStudent => {
  const newStudent = { ...createStudentSchema, id: ObjectID().toHexString() };
  students.push(newStudent);

  return newStudent;
};

export const updateStudentById = (
  studentId: string,
  updateStudentSchema: Partial<IStudent>,
): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];

  if (!student) {
    return;
  }

  const updatedStudent = { ...student, ...updateStudentSchema };

  students.splice(studentIndex, 1, updatedStudent);

  return updatedStudent;
};

export const deleteStudentById = (studentId: string): IStudent | undefined => {
  const studentIndex = students.findIndex(({ id }) => id === studentId);
  const student = students[studentIndex];

  if (!student) {
    return;
  }

  students.splice(studentIndex, 1);

  return student;
};

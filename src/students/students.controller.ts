import { Request, Response } from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentUpdateRequest } from './types/student-update-request.interface';
import { IStudentCreateRequest } from './types/student-create-request.interface';

export const getStudentById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.getStudentById(id);
  response.status(201).json(student);
};

export const getAllStudentsWithGroupName = (
  request: Request,
  response: Response,
) => {
  const students = studentsService.getAllStudentsWithGroupName();
  response.status(201).json(students);
};

export const createStudent = (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = studentsService.createStudent(request.body);
  response.status(201).json(student);
};

export const updateStudentById = (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.updateStudentById(id, request.body);
  response.status(201).json(student);
};

export const addImage = (
  request: Request<{ id: string; file: Express.Multer.File }>,
  response: Response,
) => {
  const { id } = request.params;
  const { path } = request.file ?? {};
  const student = studentsService.addImage(id, path);
  response.status(201).json(student);
};

export const getStudentImage = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const studentImage = studentsService.getStudentImage(id);
  response.status(201).json(studentImage);
};

export const addStudentToGroup = (
  request: Request<{ studentId: string; groupId: string }>,
  response: Response,
) => {
  const { studentId, groupId } = request.params;
  const student = studentsService.addStudentToGroup(studentId, groupId);
  response.status(201).json(student);
};

export const deleteStudentById = (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = studentsService.deleteStudentById(id);
  response.status(201).json(student);
};

import ObjectID from 'bson-objectid';
import HttpStatuses from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import * as studentsModel from './students.model';
import * as groupsModel from '../groups/groups.model';
import { IStudent, IStudentWithGroupName } from './types/student.interface';
import path from 'path';
import fs from 'fs/promises';

export const getAllStudentsWithGroupName = (): IStudentWithGroupName[] => {
  const studentsWithGroupName: IStudentWithGroupName[] = studentsModel
    .getAllStudents()
    .map((student) => {
      const group = student.groupId
        ? groupsModel.getGroupById(student.groupId)
        : null;
      const groupName = group ? group.name : null;
      const studentWithGroupName: IStudentWithGroupName = {
        ...student,
        groupName,
      };
      return student.groupId === null
        ? studentsModel.addInitialGroupName(studentWithGroupName)
        : studentWithGroupName;
    });
  return studentsWithGroupName;
};

export const getStudentById = (id: string) => {
  const student = studentsModel.getStudentById(id);

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }
  const group = student.groupId
    ? groupsModel.getGroupById(student.groupId)
    : null;
  const groupName = group ? group.name : null;

  return { ...student, groupName };
};

export const createStudent = (studentCreateSchema: Omit<IStudent, 'id'>) => {
  return studentsModel.createStudent(studentCreateSchema);
};

export const updateStudentById = (
  id: string,
  studentUpdateSchema: Partial<IStudent>,
) => {
  return studentsModel.updateStudentById(id, studentUpdateSchema);
};

export const addImage = async (id: string, filePath?: string) => {
  if (!filePath) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'File is not provided');
  }

  try {
    const imageId = ObjectID().toHexString();
    const imageExtension = path.extname(filePath);
    const imageName = imageId + imageExtension;

    const studentsDirectoryName = 'students';
    const studentsDirectoryPath = path.join(
      __dirname,
      '../',
      'public',
      studentsDirectoryName,
    );
    const newImagePath = path.join(studentsDirectoryPath, imageName);
    const imagePath = `${studentsDirectoryName}/${imageName}`;

    await fs.rename(filePath, newImagePath);

    const updatedStudent = updateStudentById(id, { imagePath });

    return updatedStudent;
  } catch (error) {
    await fs.unlink(filePath);
    throw error;
  }
};

export const getStudentImage = (id: string) => {
  const student = studentsModel.getStudentById(id);
  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }
  if (!student.imagePath) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student image not found');
  }
  return student.imagePath;
};

export const addStudentToGroup = (studentId: string, groupId: string) => {
  const student = studentsModel.getStudentById(studentId);
  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  const group = groupsModel.getGroupById(groupId);
  if (!group) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Group not found');
  }

  if (student.groupId === groupId) {
    throw new HttpException(
      HttpStatuses.CONFLICT,
      'Student already belongs to this group',
    );
  }

  const updatedStudent = studentsModel.updateStudentById(studentId, {
    groupId,
  });
  return updatedStudent;
};

export const deleteStudentById = (id: string) => {
  return studentsModel.deleteStudentById(id);
};

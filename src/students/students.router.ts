import { Router } from 'express';
import * as studentsController from './students.controller';
import controllerWrapper from '../application/unilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import {
  studentCreateSchema,
  studentUpdateSchema,
  addStudentToGroupSchema,
} from './student.schema';
import { idParamSchema } from '../application/schemas/id-param-schema';
import uploadMiddleware from '../application/middlewares/upload.middleware';

const router = Router();

router.get(
  '/',
  controllerWrapper(studentsController.getAllStudentsWithGroupName),
);

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentById),
);

router.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentById),
);

router.patch(
  '/:id/image',
  uploadMiddleware.single('file'),
  controllerWrapper(studentsController.addImage),
);

router.get(
  '/:id/image',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentImage),
);

router.patch(
  '/:studentId/group/:groupId',
  validator.params(addStudentToGroupSchema),
  controllerWrapper(studentsController.addStudentToGroup),
);

router.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.deleteStudentById),
);

export default router;

import { NextFunction, Request, Response } from 'express';
import HttpException from '../exceptions/http-exception';
import HttpStatuses from '../enums/http-statuses.enum';

export const handleInvalidPath = (
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  const error = new HttpException(
    HttpStatuses.NOT_FOUND,
    'The URL you are trying to reach not found',
  );
  next(error);
};

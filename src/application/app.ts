import express from 'express';
import cors from 'cors';
import logger from './middlewares/logger.middleware';
import studentRouter from '../students/students.router';
import groupRouter from '../groups/groups.router';
import bodyParser from 'body-parser';
import exceptionsFilter from './middlewares/exceptions.filter';
import path from 'path';
import { handleInvalidPath } from './middlewares/handleInvalidPath.middleware';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);

const staticFilePath = path.join(__dirname, '../');
const staticImagePath = path.join(__dirname, '../public');

app.use('/public', express.static(staticImagePath));

app.use('/api/v1/public', express.static(staticFilePath));
app.use('/api/v1/students', studentRouter);
app.use('/api/v1/groups', groupRouter);

app.use(handleInvalidPath);
app.use(exceptionsFilter);

export default app;
